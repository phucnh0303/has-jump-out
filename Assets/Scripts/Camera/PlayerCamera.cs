using System;
using DG.Tweening;
using DoublePCore.Entity;
using DoublePCore.Utils;
using UnityEngine;
using UnityEngine.Rendering.Universal;

[RequireComponent(typeof(Camera))]
public class PlayerCamera : BaseMono
{
    public CameraConfig CameraConfig;
    
    private CameraStateController stateController;
    private bool isInited = false;

    public override void Initialize()
    {
        base.Initialize();
        CreateStateMachine();
        RegisterChangeStateAction();
        isInited = true;
    }

    private void CreateStateMachine()
    {
        stateController = new CameraStateController();
        stateController.InitStates
        (
            new CameraStateIdle(this),
            new CameraStateFollowPlayer(this),
            new CameraStateMoveDown(this)
        );
    }

    private void RegisterChangeStateAction()
    {
        Observer.PlayerMoving += ChangeStateFollowPlayer;
        Observer.PlayerLanding += ChangeStateMoveDown;
        Observer.PlayerPreparingToFly += ChangeStateIdle;
    }

    #region Event Handlers
    private void ChangeStateFollowPlayer()
    {
        stateController.ChangeState<CameraStateFollowPlayer>();
    }

    private void ChangeStateMoveDown()
    {
        stateController.ChangeState<CameraStateMoveDown>();
    }

    private void ChangeStateIdle()
    {
        stateController.ChangeState<CameraStateIdle>();
    }
    #endregion
    
    public override void LateTick()
    {
        base.LateTick();
        if (isInited)
        {
            stateController.LateTick();
        }
    }

    public Vector3 GetNewCameraPos()
    {
        var cam = Camera.main;
        if (cam != null)
        {
            Vector3 screenPoint = new Vector3(Screen.width / 2, Screen.height / 6, cam.nearClipPlane);
            Vector3 worldPoint = cam.ScreenToWorldPoint(screenPoint);
            return new Vector3(worldPoint.x, worldPoint.y, 0f);
        }
        return Vector3.zero;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, CameraConfig.focusRadius);
    }

    private void OnDestroy()
    {
        UnregisterChangeStateAction();
    }

    private void UnregisterChangeStateAction()
    {
        Observer.PlayerMoving -= ChangeStateFollowPlayer;
        Observer.PlayerLanding -= ChangeStateMoveDown;
        Observer.PlayerPreparingToFly -= ChangeStateIdle;
    }
}

[Serializable]
public class CameraConfig
{
    [Min(0f)] public float focusRadius;
    public Transform focus;
    [Range(1, 20f)] public float distance = 5f;
    [ConditionalShow("isUseSeeking", false)][Range(0f, 1f)] public float focusCentering = 0.5f;
    [Range(0f, 200)] public float yOffset = 1.5f;
    
    [Header("Anim")]
    public Ease easeWhenMoveDown = Ease.Linear;
    public float durationMoveDown = 1f;
    
    [Header("Seeking")] 
    public bool isUseSeeking = true;
    [ConditionalShow("isUseSeeking", true)]public float cameraSpeed;
    [ConditionalShow("isUseSeeking", true)][Range(1f, 20f)] public float mass = 10;
    
    [HideInInspector] public PlayerController Player => focus.GetComponent<PlayerController>();
}