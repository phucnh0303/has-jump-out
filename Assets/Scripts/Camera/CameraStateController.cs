using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using DoublePCore.StateMachine;
using UnityEngine;
using UnityEngine.Rendering;

public class CameraStateController : StateMachine
{
}

public class CameraState : State
{
    protected PlayerCamera PlayerCamera;

    protected CameraState(PlayerCamera playerCamera)
    {
        this.PlayerCamera = playerCamera;
    }
}

public class CameraStateIdle : CameraState
{
    public CameraStateIdle(PlayerCamera playerCamera) : base(playerCamera)
    {
    }

    protected override void OnStateEnter(State from, object data)
    {
        base.OnStateEnter(from, data);
    }

    protected override void OnStateExit(State to)
    {
        base.OnStateExit(to);
    }
}

public class CameraStateFollowPlayer : CameraState
{
    private Vector2 focusPoint;
    private Vector2 currentVel;

    public CameraStateFollowPlayer(PlayerCamera playerCamera) : base(playerCamera)
    {
    }

    protected override void OnStateEnter(State from, object data)
    {
        base.OnStateEnter(from, data);
        focusPoint = PlayerCamera.transform.position;
        currentVel = ((Vector2)PlayerCamera.CameraConfig.focus.position - focusPoint).normalized * PlayerCamera.CameraConfig.cameraSpeed;
    }

    protected override void OnStateExit(State to)
    {
        base.OnStateExit(to);
    }

    protected override void OnStateLateUpdate()
    {
        base.OnStateLateUpdate();

        var position = PlayerCamera.transform.position;
        var cameraPos = position;
        var cameraPositionOnOxy = new Vector3(cameraPos.x, cameraPos.y, 0f);
        var distance = (cameraPositionOnOxy - PlayerCamera.CameraConfig.focus.position).magnitude;

        if (PlayerCamera.CameraConfig.isUseSeeking)
        {
            MoveCameraBySeeking(position, distance);   
        }
        else
        {
            if (distance > PlayerCamera.CameraConfig.focusRadius)
            {
                UpdateFocusPoint();
                Vector3 lookDirection = PlayerCamera.transform.forward;
                PlayerCamera.transform.localPosition = (Vector3)focusPoint - lookDirection * PlayerCamera.CameraConfig.distance;
            }
        }
    }

    private void MoveCameraBySeeking(Vector3 position, float distance)
    {
        // Use seeking for camera 
        var focusPosition = PlayerCamera.CameraConfig.focus.position;
        var vectorDirection = (Vector2)(focusPosition - position);
        
        var offset = vectorDirection - currentVel;
        var vectorVel = (offset.normalized * PlayerCamera.CameraConfig.cameraSpeed);
        if (distance < PlayerCamera.CameraConfig.focusRadius)
        {
            vectorVel *= distance / PlayerCamera.CameraConfig.focusRadius;
        }
        currentVel = vectorVel / PlayerCamera.CameraConfig.mass;
        PlayerCamera.transform.position += (Vector3)currentVel * Time.deltaTime;
    }

    private void UpdateFocusPoint()
    {
        Vector3 targetPoint = PlayerCamera.CameraConfig.focus.position;
        float distance = Vector3.Distance(targetPoint, focusPoint);
        float t = 1f;
        if (distance > 0.01f && PlayerCamera.CameraConfig.focusCentering > 0f)
        {
            t = Mathf.Pow(1f - PlayerCamera.CameraConfig.focusCentering, Time.deltaTime);
        }

        // if (distance > PlayerCamera.CameraConfig.focusRadius)
        // {
        //     //t = Mathf.Min(t, PlayerCamera.CameraConfig.focusRadius / distance);
        // }
        focusPoint = Vector3.Lerp(targetPoint, focusPoint, t);
    }
}

public class CameraStateMoveDown : CameraState
{
    public CameraStateMoveDown(PlayerCamera playerCamera) : base(playerCamera)
    {
    }

    protected override void OnStateEnter(State from, object data)
    {
        base.OnStateEnter(from, data);
        var endPosOnScreen = PlayerCamera.GetNewCameraPos();
        var endPos = PlayerCamera.transform.position + (PlayerCamera.CameraConfig.Player.CurrentCelestial.transform.position - endPosOnScreen);
        PlayerCamera.transform
            .DOMove(endPos, PlayerCamera.CameraConfig.durationMoveDown)
            .SetEase(PlayerCamera.CameraConfig.easeWhenMoveDown)
            .OnComplete(() => { Observer.PlayerPreparingToFly?.Invoke(); });
    }

    protected override void OnStateExit(State to)
    {
        base.OnStateExit(to);
    }
}