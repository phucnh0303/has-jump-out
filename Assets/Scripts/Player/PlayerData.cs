using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Celestial/Player Data")]
public class PlayerData : CelestialData
{
    [Header("Player Data")]
    public int maxForce;
}
