using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using DG.Tweening;
using DoublePCore.StateMachine;
using UnityEngine;

public class PlayerStateController : StateMachine
{
}

public abstract class PlayerState : State
{
    protected readonly PlayerController Player;

    protected PlayerState(PlayerController player)
    {
        this.Player = player;
    }
}

public class PlayerStateMoving : PlayerState
{
    public PlayerStateMoving(PlayerController player) : base(player)
    {
    }

    protected override void OnStateEnter(State from, object data)
    {
        base.OnStateEnter(from, data);
        Player._rb.constraints = RigidbodyConstraints2D.None;
        Player._rb.AddForce(Player.transform.up * GameManager.Instance.Force * 5, ForceMode2D.Impulse);
    }
}

public class PlayerStateLanding : PlayerState
{
    public PlayerStateLanding(PlayerController player) : base(player)
    {
    }

    protected override void OnStateEnter(State from, object data)
    {
        base.OnStateEnter(from, data);
        //Player.transform.SetParent(Player.CurrentCelestial.transform);
    }
}

public class PlayerStatePrepareFLy : PlayerState
{
    private GameObject rotateWithCelestial;
    public PlayerStatePrepareFLy(PlayerController player) : base(player)
    {
    }

    protected override async void OnStateEnter(State from, object data)
    {
        if (StateMachine.CurrentState != from)
        {
            base.OnStateEnter(from, data);

            rotateWithCelestial = Object.Instantiate(new GameObject(), Player.transform.position, Quaternion.identity);
            rotateWithCelestial.transform.SetParent(Player.CurrentCelestial.transform);
            
            await Player.CurrentCelestial.RotateOnPreparingToFly(rotateWithCelestial.transform.localPosition);
            Player.transform.up = Vector2.up;
            Observer.ShowQuestion?.Invoke();
        }
    }

    protected override void OnStateUpdate()
    {
        base.OnStateUpdate();
        if (rotateWithCelestial != null)
        {
            var transform = Player.transform;
            transform.position = rotateWithCelestial.transform.position;
            transform.up = (transform.position - Player.CurrentCelestial.transform.position).normalized;
        }
    }

    protected override void OnStateExit(State to)
    {
        base.OnStateExit(to);
        Object.Destroy(rotateWithCelestial);
    }
}

public class PlayerStartState : PlayerState
{
    public PlayerStartState(PlayerController player) : base(player)
    {
    }

    protected override void OnStateEnter(State from, object data)
    {
        base.OnStateEnter(from, data);
        Player._rb.AddForce(Vector2.up, ForceMode2D.Impulse);
    }
}

public class PlayerStateLandingAndWaitForCamera : PlayerState
{
    public PlayerStateLandingAndWaitForCamera(PlayerController player) : base(player)
    {
    }

    protected override async void OnStateEnter(State from, object data)
    {
        base.OnStateEnter(from, data);
        Player._rb.constraints = RigidbodyConstraints2D.FreezeAll;
        // Hard code for wait camera
        await Task.Delay(1500);
        Observer.PlayerLanding?.Invoke();
    }
}

public class PlayerStateCompletedRace : PlayerState
{
    public PlayerStateCompletedRace(PlayerController player) : base(player)
    {
    }
}