using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using DG.Tweening;
using DoublePCore.Entity;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : BaseNetwork, IGravity
{
    [Header("Data")] 
    [SerializeField] private PlayerData data;
    [Header("Info")] 
    [SerializeField] private TMP_Text playerName;

    [HideInInspector] public Rigidbody2D _rb => GetComponent<Rigidbody2D>();

    private PlayerStateController stateController;

    public PlayerData Data => data;
    public BaseCelestial CurrentCelestial { get; private set; }

    public override void Initialize()
    {
        base.Initialize();
        if (IsOwner)
        {
            // Double check for safe
            RegisterEvent();
            CreateStateMachine();
        }
    }

    private void CreateStateMachine()
    {
        stateController = new PlayerStateController();
        stateController.InitStates
        (
            new PlayerStartState(this),
            new PlayerStateLanding(this),
            new PlayerStateMoving(this),
            new PlayerStatePrepareFLy(this),
            new PlayerStateLandingAndWaitForCamera(this)
        );

        stateController.ChangeState<PlayerStartState>();
    }

    private void RegisterEvent()
    {
        Observer.PlayerLanding += OnPlayerLanding;
        Observer.PlayerMoving += OnPlayerMoving;
        Observer.PlayerPreparingToFly += OnPlayerPreparingToFly;
        Observer.PlayerLandingAndWaitForCamera += OnPlayerLandingAndWaitForCamera;
    }

    public override void Tick()
    {
        base.Tick();
        stateController.Tick();
    }

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        playerName.SetText($"Player: {OwnerClientId.ToString()}");
        if (!IsOwner)
        {
            GetComponent<BoxCollider2D>().enabled = false;
            DestroyImmediate(_rb);
            enabled = false;
            return;
        }
        GameManager.Instance.SetupForOwner(this);
    }

    public void GravityEffect(Vector2 force)
    {
        _rb.AddForce(force, ForceMode2D.Force);
    }

    public CelestialData GetData()
    {
        return data;
    }

    #region Event Handlers

    private void OnPlayerMoving()
    {
        CurrentCelestial = null;
        stateController.ChangeState<PlayerStateMoving>();
    }

    private void OnPlayerLanding()
    {
        // hidden stamina ui
        GameManager.Instance.img.fillAmount = 0;
        stateController.ChangeState<PlayerStateLanding>();
    }

    private void OnPlayerPreparingToFly()
    {
        stateController.ChangeState<PlayerStatePrepareFLy>();
    }

    private void OnPlayerLandingAndWaitForCamera()
    {
        stateController.ChangeState<PlayerStateLandingAndWaitForCamera>();
    }

    #endregion

    #region Interact With Celestial

    public void InteractWithNormalCelestial(BaseCelestial celestial)
    {
        if (CurrentCelestial != celestial)
        {
            CurrentCelestial = celestial;
            Observer.PlayerLandingAndWaitForCamera?.Invoke();
        }
    }

    public void InteractWithBlackHole(BaseCelestial celestial)
    {
        // fall into the hole
        // teleport to nearest white hole 
    }

    public void InteractWithWhiteHole(BaseCelestial celestial)
    {
        // add -force 
    }

    #endregion

    public override void OnDestroy()
    {
        base.OnDestroy();
        UnRegisterEvent();
    }

    private void UnRegisterEvent()
    {
        Observer.PlayerLanding -= OnPlayerLanding;
        Observer.PlayerMoving -= OnPlayerMoving;
        Observer.PlayerPreparingToFly -= OnPlayerPreparingToFly;
        Observer.PlayerLandingAndWaitForCamera -= OnPlayerLandingAndWaitForCamera;
    }
}