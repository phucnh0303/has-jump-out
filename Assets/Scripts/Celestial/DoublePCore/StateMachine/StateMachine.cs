using System.Linq;
using UnityEngine;
using UnityEngine.Rendering;

namespace DoublePCore.StateMachine
{
    public class StateMachine 
    {
        /// <summary>
        /// If you use it with tick, fixedTick or lateTick
        /// Remember to call Tick(), FixedTick() or LateTick() in script create state machine
        /// </summary>
        public State CurrentState { get; private set; }

        public State[] States { get; private set; }

        public void InitStates(params State[] states)
        {
            States = states;
            foreach (var state in States)
            {
                state.StateMachine = this;
            }
        }

        public void ChangeState<T>(object data = null) where T : State
        {
            var state = States.FirstOrDefault(s => s is T);
            ChangeState(state, data);
        }

        void ChangeState(State state, object data = null)
        {
            if (state == CurrentState) return;
            
            var oldState = CurrentState;
            CurrentState = state;
            oldState?.StateExit(state);
            state?.StateEnter(oldState, data);
        }

        public void Tick()
        {
            CurrentState?.StateTick();
        }

        public void FixedTick()
        {
            CurrentState?.StateFixedTick();
        }

        public void LateTick()
        {
            CurrentState?.StateLateTick();
        }
    }
}
