using UnityEngine;

namespace DoublePCore.StateMachine
{
    public class State 
    {
        public global::DoublePCore.StateMachine.StateMachine StateMachine { get; set; }

        public void ChangeState<T>() where T : State
        {
            StateMachine.ChangeState<T>();
        }
        
        public void StateEnter(State from, object data)
        {
            OnStateEnter(from, data);
        }

        public void StateTick()
        {
            OnStateUpdate();
        }

        public void StateFixedTick()
        {
            OnStateFixedUpdate();
        }

        public void StateLateTick()
        {
            OnStateLateUpdate();
        }

        public void StateExit(State to)
        {
            OnStateExit(to);
        }

        protected virtual void OnStateEnter(State from, object data)
        {
        }

        protected virtual void OnStateUpdate()
        {
        }

        protected virtual void OnStateFixedUpdate()
        {
        }
        
        protected virtual void OnStateLateUpdate()
        {
        }

        protected virtual void OnStateExit(State to)
        {
        }
    }
}
