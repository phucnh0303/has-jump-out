using System.Collections;
using System.Collections.Generic;
using DoublePCore.SO;
using UnityEngine;

[CreateAssetMenu(menuName = "Celestial/Celestial Data")]
public class CelestialData : BaseSO
{
    [Header("Celestial Data")]
    public float mass;
    [Range(0, 5)] public float gravityEffectRange;
}
