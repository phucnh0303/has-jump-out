using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackHole : BaseCelestial
{
    protected override void OnCollisionWithIGravity(IGravity iGravity)
    {
        iGravity.InteractWithBlackHole(this);
    }
}
