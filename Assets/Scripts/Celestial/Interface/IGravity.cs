using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGravity
{
    public void GravityEffect(Vector2 force);
    public CelestialData GetData();
    public void InteractWithNormalCelestial(BaseCelestial celestial);
    public void InteractWithBlackHole(BaseCelestial celestial);
    public void InteractWithWhiteHole(BaseCelestial celestial);
}
