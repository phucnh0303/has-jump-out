using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UIElements;

public interface IRotate
{
    public TaskCompletionSource<bool> GetAsync();
    public void Rotate(Vector2 playerPosition, GameObject thisCelestial);
}
