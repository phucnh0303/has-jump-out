using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using DG.Tweening;
using UnityEngine;

public class RotateWhenPreparingToFly : IRotate
{
    private TaskCompletionSource<bool> tcs;
    
    public TaskCompletionSource<bool> GetAsync()
    {
        return tcs;
    }

    public void Rotate(Vector2 playerPosition, GameObject thisCelestial)
    {
        tcs = new TaskCompletionSource<bool>();
        var currentDir = playerPosition.normalized;
        var newDir = Vector2.up;
        var deltaAngle = Vector2.SignedAngle(from: currentDir, to: newDir);
        var angleNormalized = deltaAngle - Mathf.Floor(deltaAngle / 360) * 360;
        // Start rotating
        thisCelestial.transform
            .DOLocalRotate(new Vector3(0, 0, angleNormalized), 1f, RotateMode.FastBeyond360)
            .OnComplete(CompleteTask);
    }

    private void CompleteTask()
    {
        tcs.SetResult(true);
    }
}
