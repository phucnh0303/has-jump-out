using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using DoublePCore.Entity;
using UnityEngine;
using UnityEngine.UIElements;

public abstract class BaseCelestial : BaseMono
{
    [SerializeField] private CelestialData data;

    [SerializeField] private LayerMask mask;

    private Collider2D prehit;
    private Collider2D hit;
    protected IGravity iGravity;
    protected IRotate iRotate;

    public CelestialData Data => data;

    public override void Tick()
    {
        base.Tick();
        hit = Physics2D.OverlapCircle(transform.position, data.gravityEffectRange, mask);
    }

    public override void FixedTick()
    {
        base.FixedTick();
        ReactWithIGravity();
    }

    protected virtual void ReactWithIGravity()
    {
        if (hit != null)
        {
            if (hit != prehit)
            {
                prehit = hit;
                iGravity = hit.GetComponent<IGravity>();
            }
            
            if (iGravity != null)
            {
                var position = transform.position;
                var hitPos = hit.transform.position;
                var forceValue = ForceUtils.CalculateGravity
                (
                    new KeyValuePair<Vector2, CelestialData>(position, data),
                    new KeyValuePair<Vector2, CelestialData>(hitPos, iGravity.GetData())
                );
                var dir = (position - hitPos).normalized;
                
                iGravity.GravityEffect(dir * forceValue);
            }
        }
    }

    public virtual async Task RotateOnPreparingToFly(Vector2 playerPosition)
    {
        if (iRotate == null) return;
        iRotate.Rotate(playerPosition, this.gameObject);
        await iRotate.GetAsync().Task;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        other.gameObject.TryGetComponent<IGravity>(out var iGravity);
        if (iGravity != null)
        {
            OnCollisionWithIGravity(iGravity);
        }
    }

    protected abstract void OnCollisionWithIGravity(IGravity iGravity);

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, data.gravityEffectRange);
    }
}