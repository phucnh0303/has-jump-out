using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhiteHole : BaseCelestial
{
    protected override void OnCollisionWithIGravity(IGravity iGravity)
    {
        iGravity.InteractWithWhiteHole(this);
    }
}
