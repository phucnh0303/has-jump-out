using System;
using System.Linq;
using DoublePCore.Entity;
using UnityEngine;

public class NormalCelestial : BaseCelestial
{
    public override void Initialize()
    {
        base.Initialize();
        iRotate = new RotateWhenPreparingToFly();
    }

    protected override void OnCollisionWithIGravity(IGravity iGravity)
    {
        iGravity.InteractWithNormalCelestial(this);
    }
}
