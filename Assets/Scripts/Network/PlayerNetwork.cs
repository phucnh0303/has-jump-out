using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class PlayerNetwork : BaseNetwork
{
    private readonly NetworkVariable<PlayerNetworkData> playerData = new NetworkVariable<PlayerNetworkData>(writePerm: NetworkVariableWritePermission.Owner);

    [SerializeField] private float cheapInterpolationTime = 0.1f;
    
    private Vector3 _vel;

    public override void Tick()
    {
        base.Tick();
        if (IsOwner)
        {
            // Send data to server
            playerData.Value = new PlayerNetworkData()
            {
                Pos = transform.position
            };
        }
        else
        {
            Debug.Log("Testing not owner");
            // Read data from server and use it for this player
            transform.position = Vector3.SmoothDamp(transform.position, playerData.Value.Pos, ref _vel, cheapInterpolationTime);
        }
    }
}

struct PlayerNetworkData : INetworkSerializable
{
    private float x;
    private float y;

    internal Vector3 Pos
    {
        get => new Vector2(x, y);
        set
        {
            x = value.x;
            y = value.y;
        }
    }


    public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
    {
        serializer.SerializeValue(ref x);
        serializer.SerializeValue(ref y);
    }
}
