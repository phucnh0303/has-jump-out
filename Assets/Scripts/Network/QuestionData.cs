using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class QuestionData
{
    public string question;
    public AnswerEnum correctAnswer;
    public AnswerData[] answerData;
}

[Serializable]
public class AnswerData
{
    public string content;
    public AnswerEnum answer;
}

[Serializable]
public enum AnswerEnum
{
    A,
    B,
    C,
    D
}