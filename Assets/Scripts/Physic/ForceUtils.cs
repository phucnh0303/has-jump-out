using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public static class ForceUtils
{
    public static float CalculateGravity(KeyValuePair<Vector2, CelestialData> object1, KeyValuePair<Vector2, CelestialData> object2)
    {
        var dir = (object2.Key - object1.Key).normalized;
        var forceValue = CONST.G * (object1.Value.mass * object2.Value.mass) / Mathf.Pow(dir.magnitude, 2);

        return forceValue;
    }
}

[Serializable]
public class CONST
{
    public static float G = 0.3f;
}