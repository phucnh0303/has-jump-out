using System;
using UnityEngine;

public static class Observer
{
    #region Network

    public static Action SpawningPlayer;

    #endregion // -> when server is spawning player

    #region Player State

    public static Action PlayerMoving; // -> camera state follow player
    public static Action PlayerLanding; // -> camera state move down
    public static Action PlayerPreparingToFly; // -> camera state idle
    public static Action PlayerLandingAndWaitForCamera; // -> camera state unChange until movement to celestial done

    #endregion

    #region Gameplay State

    public static Action ShowQuestion;

    #endregion
}