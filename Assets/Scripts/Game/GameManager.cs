using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : DoublePCore.Singleton.Singleton<GameManager>
{
    public PlayerCamera playerCamera;
    public PlayerController Player { get; set; }
    public UIController UIController;

    public void SetupForOwner(PlayerController player)
    {
        Player = player;
        playerCamera.CameraConfig.focus = Player.transform;
        playerCamera.Initialize();
    }
    
    #region Input

    public Image img => UIController.imgStamina;
    
    private bool isPressed = false;
    private readonly WaitForSeconds timeDelay = new WaitForSeconds(0.005f);
    private float force;
    private int dir;

    public float Force => force / Player.Data.maxForce;

    public void OnPressedButtonBegin()
    {
        isPressed = true;
        force = 0f;
        dir = 1;

        StartCoroutine(ChangeStamina());
    }

    public void OnPressedButtonEnd()
    {
        isPressed = false;
        Observer.PlayerMoving?.Invoke();
    }

    private IEnumerator ChangeStamina()
    {
        while (isPressed)
        {
            img.fillAmount = Force;
            if (force > Player.Data.maxForce) dir = -1;
            if (force < 0.1f) dir = 1;

            force += Time.deltaTime * 50 * dir;
            yield return timeDelay;
        }
        yield return null;
    }

    #endregion
}
