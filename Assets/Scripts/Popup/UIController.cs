using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DoublePCore.Entity;
using Ricimi;
using UnityEngine;
using UnityEngine.UI;

public class UIController : BaseMono
{
    public Image imgStamina;
    
    [SerializeField] private Transform container;
    
    [Header("Popups")]
    [SerializeField] private PopupQuestion popupQuestion;

    public override void Initialize()
    {
        base.Initialize();
        Observer.ShowQuestion += ShowQuestion;
    }

    private void ShowQuestion()
    {
        popupQuestion.gameObject.SetActive(true);
    }
    
    private void OnDestroy()
    {
        Observer.ShowQuestion -= ShowQuestion;
    }
}
