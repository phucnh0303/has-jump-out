using System.Collections;
using System.Collections.Generic;
using DoublePCore.Entity;
using Ricimi;
using UnityEditor.VersionControl;
using UnityEngine;
using Task = System.Threading.Tasks.Task;

public class BasePopup : BaseMono
{
   [SerializeField] private Popup popup;
   [SerializeField] private GameObject bg;
   [SerializeField] private GameObject container;

   public override void DoEnable()
   {
      base.DoEnable();
      bg.SetActive(true);
      container.SetActive(true);
   }

   protected virtual void OnOpened()
   {
   }
   
   public virtual void OnClosed()
   {
      ClearData();
      bg.SetActive(false);
      gameObject.SetActive(false);
   }

   private void ClearData()
   {
      // Clear all previous data  
   }
}
